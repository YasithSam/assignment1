#include <stdio.h>

//Swap function declaration
void swap(int* a, int* b); 

int main()
{
   int x, y;

   printf("Enter the first number as x:\n");
   scanf("%d",&x);
   
   printf("Enter the first number as y:\n");
   scanf("%d",&y);

   
   printf("Before swapping\nx = %d\ny = %d\n",x,y);

   //passing pointers of x & y to swap function
   swap(&x, &y);
   
   
   printf("After Swapping\nx = %d\ny = %d\n", x, y);

   return 0;
}

//Swap function definition
void swap(int *a, int *b)
{
   //declaring third variable
   int c;

   c = *b;
   *b = *a;
   *a = c;
}
