#include <stdio.h>

int main(){
   double x, y, product;
   
    //user input first number
    printf("Enter the first number: ");
    scanf("%lf", &x);  
    
    //user input second number
    printf("Enter the second number: ");
    scanf("%lf", &y); 
 
    // Calculating product
    product = x * y;

    // Result up to 2 decimal point is displayed using %.2lf
    printf("Product = %.2lf\n", product);
    
    return 0;;
 
}
