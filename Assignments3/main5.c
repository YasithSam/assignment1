#include<stdio.h>
/*Multipication table upto a given number*/

int main(int argc, char *argv[]){


    int n, r, c;
    // User input
    printf("Enter a number :");
    scanf("%d", &n);

    
    for (r = 1; r <= n; r++) { 
        printf("multipication table of %d\n",r);
        for (c = 1; c <= 12; c++) { 
            printf("%d * %d = %d\t ", r, c, r * c); 
        }
        printf("\n\n");
    }

    return 0;
}

