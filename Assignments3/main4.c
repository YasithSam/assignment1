#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	
    int num,i;
    printf("Type the number to find the factors: ");
    scanf("%d",&num);
    
    if(num==0){
        
        printf("Every number is a factor of zero");
    }
    printf("Factors of the given number are:");
    
    /*Checking factors of positive numbers*/
    
    
    for(i=1;i<=num;i++){
        
        if(num%i==0){
           printf("%d ",i);
        }    
    }
    
    
    

    return 0;
}
