#include<stdio.h>

/*Finding the factors of a given number*/


int main(int argc, char *argv[]) {
	
   int i, num, p = 0;
   
   printf("Please enter a number: \n");
   scanf("%d", &num);
   for(i=2; i<=num; i++)
   {
      if(num%i==0)
      {
         p++;
      }
   }
   if(p==1)
   {
      printf("Entered number is %d and it is a primary number",num);
   }
   else
   {
      printf("Entered number is %d "\
             "and it is not a prime number.",num);
   }
   return 0;
}