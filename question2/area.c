#include<stdio.h>
#define pi 3.14

//declaring the function
double findArea(float r);

int main()
{
     // declare variables
     float radius;
     double area;

     // take inputs
     printf("Enter Radius of Circle in cm: ");
     scanf("%f", &radius);

     // calling function to calculate area
     area = findArea(radius);
     

     // display result
     printf("Area of Circle = %.2lf cm\n",area);

     return 0;
}

//defining the function 
double findArea(float r){
    
   	return (pi * r * r);
	
	
}