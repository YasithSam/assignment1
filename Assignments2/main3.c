#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {

    char c;

   
    printf("Enter any character: ");
    scanf("%c", &c);


    /* Condition for vowel */
    if(c==97 || c==101 || c==105 || c==111 || c==117 || 
       c==65 || c==69  || c==73  || c==79  || c==85)
    {
        printf("'%c' is Vowel.", c);
    }
    /* Condition for consonant*/
    else if((c >= 97 && c <= 122) || (c >= 65 && c <= 90))
    {
        
        printf("'%c' is Consonant.", c);
    }
    /* Neither Consonant or alphabet then it is non alphabetic*/
    else 
    {
        
        printf("%c is not an alphabet character", c);
    }

    return 0;
}
