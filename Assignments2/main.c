#include <stdio.h>
double check(double x);
int main(int argc, char *argv[]) {
    double num;
    printf("Enter a number: ");
    scanf("%lf", &num);
    check(num);
    
    return 0;
}
double check(double x){
	if (x > 0.0)
        printf("You entered a positive number");
    else if (x < 0.0)
        printf("You entered a negative number");
    else
        printf("You entered 0");
}
